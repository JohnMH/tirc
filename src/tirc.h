/*
 * Copyright 2016 John M. Harris, Jr.
 *
 * This file is part of tIRC.
 *
 * tIRC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tIRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with tIRC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TIRC_H_
#define TIRC_H_

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define VER_MAJOR 0
#define VER_MINOR 1
#define VER_PATCH 1

#define VER_STR STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_PATCH)

#endif
