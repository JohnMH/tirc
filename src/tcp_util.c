/*
 * Copyright 2016 John M. Harris, Jr.
 *
 * This file is part of tIRC.
 *
 * tIRC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tIRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with tIRC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "tcp_util.h"

#include <stdio.h>
#include <string.h>
#include <strings.h>

#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/errno.h>

int tcp_util_connect(char* addr, char* port){
	int ret;
	struct addrinfo hints;
	struct addrinfo* servinfo;

	bzero(&hints, sizeof(struct addrinfo));

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	if((ret = getaddrinfo(addr, port, &hints, &servinfo)) == -1){
		fprintf(stderr, "Failed to resolve: %s\n", gai_strerror(ret));
		return -1;
	}

	struct addrinfo* p;

	char ipstr[INET_ADDRSTRLEN];

	int sockfd;

	for(p = servinfo; p != NULL; p = p->ai_next){
		struct in_addr* addr;
		if(p->ai_family == AF_INET){
			struct sockaddr_in* ipv = (struct sockaddr_in*)p->ai_addr;
			addr = &(ipv->sin_addr);
		}else{
			struct sockaddr_in6* ipv6 = (struct sockaddr_in6*)p->ai_addr;
			addr = (struct in_addr*) &(ipv6->sin6_addr);
		}

		inet_ntop(p->ai_family, addr, ipstr, sizeof(ipstr));

		printf("Attempting to connect: %s:%s\n", ipstr, port);

		sockfd = socket(p->ai_family, SOCK_STREAM, 0);
		if(sockfd != -1){
			if(connect(sockfd, p->ai_addr, p->ai_addrlen) == 0){
				return sockfd;
			}
		}else{
			puts("Failed to create socket.");
		}
	}

	puts("Failed to connect.");

	return -1;
}
