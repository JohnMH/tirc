/*
 * Copyright 2016 John M. Harris, Jr.
 *
 * This file is part of tIRC.
 *
 * tIRC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tIRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with tIRC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TLS_MODE_H_
#define TLS_MODE_H_

#include "tirc_mode.h"

#include <gnutls/gnutls.h>

struct tirc_mode_tls_init_ud{
	char* hostname;
	int verifyCerts;
};

struct tirc_mode_tls_ud{
	gnutls_session_t session;
	int sockfd;
};

/*
 * Initializes the TLS transport mode, and returns a
 * struct of type tirc_mode_tls_ud.
 */
void* tirc_mode_tls_init(void* init_ud);

/*
 * Opens a connection to the specified address,
 * on the specified port.
 * ud is the userdata returned from tirc_mode_tls_init
 */
int tirc_mode_tls_open(void* ud, char* addr, char* port);

/*
 * Writes data to a specified connection.
 * ud is the userdata returned from tirc_mode_tls_init
 */
int tirc_mode_tls_write(void* ud, char* buf, int bufsize);

/*
 * Reads data from a specified connection.
 * ud is the userdata returned from tirc_mode_tls_init
 */
int tirc_mode_tls_read(void* ud, char* buf, int count);

/*
 * Closes a specified connection.
 * ud is the userdata returned from tirc_mode_tls_init
 */
int tirc_mode_tls_close(void* ud);

/*
 * Returns a struct of type tirc_mode.
 * This struct will be created with the values set to
 * the functions defined by this mode.
 */
struct tirc_mode* tirc_mode_tls();

#endif
