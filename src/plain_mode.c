/*
 * Copyright 2016 John M. Harris, Jr.
 *
 * This file is part of tIRC.
 *
 * tIRC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tIRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with tIRC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "plain_mode.h"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include <unistd.h>
#include <sys/socket.h>

#include "tcp_util.h"

void* tirc_mode_plain_init(void* init_ud){
	struct tirc_mode_plain_ud* mode_ud = malloc(sizeof(struct tirc_mode_plain_ud));
	bzero(mode_ud, sizeof(struct tirc_mode_plain_ud));

	return mode_ud;
}

int tirc_mode_plain_open(void* ud, char* addr, char* port){
	struct tirc_mode_plain_ud* mode_ud = (struct tirc_mode_plain_ud*)ud;

	mode_ud->sockfd = mode_ud->sockfd = tcp_util_connect(addr, port);

	if(mode_ud->sockfd == -1){
		return 1;
	}

	return 0;
}

int tirc_mode_plain_write(void* ud, char* buf, int bufsize){
	puts("Writing!");
	puts(buf);


	struct tirc_mode_plain_ud* mode_ud = (struct tirc_mode_plain_ud*)ud;
	int ret = send(mode_ud->sockfd, buf, bufsize, 0);

	write(stdout, buf, bufsize);

	if(ret == -1){
		//Handle errno
	}

	return ret;
}

int tirc_mode_plain_read(void* ud, char* buf, int count){
	struct tirc_mode_plain_ud* mode_ud = (struct tirc_mode_plain_ud*)ud;
	int ret = recv(mode_ud->sockfd, buf, count, 0);

	return ret;
}

int tirc_mode_plain_close(void* ud){
	struct tirc_mode_plain_ud* mode_ud = (struct tirc_mode_plain_ud*)ud;
	return shutdown(mode_ud->sockfd, SHUT_RDWR);
}

struct tirc_mode* tirc_mode_plain(){
	struct tirc_mode* mode = malloc(sizeof(struct tirc_mode));
	mode->init = tirc_mode_plain_init;
	mode->open = tirc_mode_plain_open;
	mode->write = tirc_mode_plain_write;
	mode->read = tirc_mode_plain_read;
	mode->close = tirc_mode_plain_close;

	return mode;
}
