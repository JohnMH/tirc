/*
 * Copyright 2016 John M. Harris, Jr.
 *
 * This file is part of tIRC.
 *
 * tIRC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tIRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with tIRC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include <pthread.h>
#include <getopt.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "tirc.h"

#include "plain_mode.h"
#include "tls_mode.h"

#define MAX_BUFFER_LEN 1024

static char* curChan = NULL;
static char* curNick = NULL;
static int stillConnected = 1;

struct tirc_mode_info{
	struct tirc_mode* mode;
	void* ud;
};

void rl_safe_printf(char* fmt, ...){
	int need_hack = (rl_readline_state & RL_STATE_READCMD) > 0;
	char* saved_line;
	int saved_point;
	if(need_hack){
		saved_point = rl_point;
		saved_line = rl_copy_text(0, rl_end);
		rl_save_prompt();
		rl_replace_line("", 0);
		rl_redisplay();
	}

	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);

	if(need_hack){
		rl_restore_prompt();
		rl_replace_line(saved_line, 0);
		rl_point = saved_point;
		rl_redisplay();
		free(saved_line);
	}
}

void rl_safe_puts(char* s){
	int need_hack = (rl_readline_state & RL_STATE_READCMD) > 0;
	char* saved_line;
	int saved_point;
	if(need_hack){
		saved_point = rl_point;
		saved_line = rl_copy_text(0, rl_end);
		rl_save_prompt();
		rl_replace_line("", 0);
		rl_redisplay();
	}

	puts(s);

	if(need_hack){
		rl_restore_prompt();
		rl_replace_line(saved_line, 0);
		rl_point = saved_point;
		rl_redisplay();
		free(saved_line);
	}
}

static char** known_commands = {"QUIT", "JOIN", "PRIVMSG"};

char* tirc_cmd_generator(const char* txt, int state){//Completes known commands
	static int list_idx, len;
	char* name;

	if(!state){
		list_idx = 0;
		len = strlen(txt);
	}

	while((name = known_commands[list_idx])){
		list_idx++;

		if(strncmp(name, txt, len) == 0){
			return strdup(name);
		}
	}

	return NULL;
}

char* tirc_null_generator(const char* txt, int state){
	return NULL;
}

static char** tirc_completion(const char* txt, int start, int end){
	char** matches;

	matches = (char**)NULL;

	if(end != 0){
		if(txt[0] == '/'){

		}
	}

	return rl_completion_matches(txt, &tirc_null_generator);
}

void* handle_input(void* mode_ptr){
	struct tirc_mode_info* mode_info = (struct tirc_mode_info*)mode_ptr;

	rl_attempted_completion_function = tirc_completion;
	rl_bind_key('\t', rl_complete);

	while(stillConnected){
		int promptStrLen = strlen(curNick) + 3;
		char promptStr[promptStrLen];
		strcpy(promptStr, curNick);
		strcat(promptStr, "> ");
		promptStr[promptStrLen] = '\0';

		char* line = readline(promptStr);

		if(line){
			int lineLen = strlen(line);
			if(lineLen > 0){
				add_history(line);

				char sndLine[lineLen + 3];
				strcpy(sndLine, line);
				strcat(sndLine, "\r\n\0");

				free(line);

				mode_info->mode->write(mode_info->ud, sndLine, lineLen + 2);
			}
		}
	}

	return NULL;
}

int main(int argc, char** argv){
	int useTLS = 0;
	int verifyTLS = 1;

	char* connPort = "6667";
	char* addr;

	char* irc_user = NULL;
	char* irc_nick = NULL;
	char* irc_gecos = NULL;
	char* irc_pass = NULL;

	static struct option long_opts[] = {
		{"version", no_argument, 0, 'v'},
		{"help", no_argument, 0, 'h'},
		{"tls", no_argument, 0, 's'},
		{"no-tls-verification", no_argument, 0, 'N'},
		{"user", required_argument, 0, 'u'},
		{"nick", required_argument, 0, 'n'},
		{"realname", required_argument, 0, 'r'},
		{"password", required_argument, 0, 'P'},
		{"port", required_argument, 0, 'p'},
		{0, 0, 0, 0}
	};

	int opt_index = 0;

	int c;
	while(1){
		c = getopt_long(argc, argv, "vhsNu:n:r:P:p:", long_opts, &opt_index);

		if(c == -1){
			break;
		}

		switch(c){
			case 'v':{
				puts("tIRC "  VER_STR);
				puts("Copyright (C) 2016 John M. Harris, Jr.");
				puts("This is free software. It is licensed for use, modification and");
				puts("redistribution under the terms of the GNU General Public License,");
				puts("version 3 or later <https://gnu.org/licenses/gpl.html>");
				puts("");
				puts("Please send bug reports to: <johnmh@openblox.org>");
				exit(0);
				break;
			}
			case 'h':{
				puts("tIRC - simple terminal IRC client");
				puts("Usage: tirc [options] [hostname]");
				puts("");
				puts("   -u, --user=str              User name to use");
				puts("                               Defaults to the current user's username");
				puts("   -n, --nick=str              Nick name to use on the server");
				puts("                               Defaults to the user name used");
				puts("   -r, --realname=str          GECOS to use");
				puts("                               Defaults to the current user's GECOS");
				puts("   -P, --password              Server password");
				puts("   -s, --tcp                   Enables TLS on the connection");
				puts("   -N, --no-tls-verification   Disables TLS server verification");
				puts("   -p, --port=num              Specifies the port number to connect on");
				puts("                               Defaults to 6667");
				puts("   -v, --version               Prints version information and exits");
				puts("   -h, --help                  Prints this help text and exits");
				puts("");
				puts("Options are specified by doubled hyphens and their name or by a single");
				puts("hyphen and the flag character.");
				puts("");
				puts("This IRC client is just a little bit more sophisticated than a simple");
				puts("telnet client. This client automatically responds to PINGs and sends");
				puts("lines that don't start with '/', unless two '/' characters are used,");
				puts("to the last channel specified. The use of a special SETCHAN command");
				puts("is used to specify the channel to send messages to.");
				puts("");
				puts("Please send bug reports to: <johnmh@openblox.org>");
				exit(0);
				break;
			}
			case 'N': {
				verifyTLS = 0;
				break;
			}
			case 'u': {
				if(irc_user){
					free(irc_user);
					irc_user = NULL;
				}
				irc_user = strdup(optarg);
				break;
			}
			case 'n': {
				if(irc_nick){
					free(irc_nick);
					irc_nick = NULL;
				}
				irc_nick = strdup(optarg);
				break;
			}
			case 'P':{
				if(irc_pass){
					free(irc_pass);
					irc_pass = NULL;
				}
				irc_pass = strdup(optarg);
				break;
			}
			case 'r': {
				if(irc_gecos){
					free(irc_gecos);
					irc_gecos = NULL;
				}
				irc_nick = strdup(optarg);
				break;
			}
			case 's':{
				useTLS = 1;
				break;
			}
			case 'p':{
				connPort = strdup(optarg);
				break;
			}
			case '?':{
				//getopt already handled the error message
				exit(1);
				break;
			}
			default:{
				exit(1);
			}
		}
	}

	if(optind < argc){
		addr = strdup(argv[optind++]);
	}else{
		puts("No server address specified.");
		exit(1);
	}

	struct tirc_mode* cur_mode;
	void* init_ud = NULL;

	if(useTLS){
		struct tirc_mode_tls_init_ud* init_u = malloc(sizeof(struct tirc_mode_tls_init_ud));
		init_u->hostname = addr;
		init_u->verifyCerts = verifyTLS;
		init_ud = init_u;

		cur_mode = tirc_mode_tls();
		//TODO: Set init_ud to a path to a client cert
	}else{
		cur_mode = tirc_mode_plain();
	}

	void* mode_ud = cur_mode->init(init_ud);
	if(mode_ud == NULL){
		exit(1);
	}

	if(cur_mode->open(mode_ud, addr, connPort) == 1){
		exit(1);
	}

	struct tirc_mode_info* cur_mode_info = malloc(sizeof(struct tirc_mode_info));
	bzero(cur_mode_info, sizeof(struct tirc_mode_info));

	cur_mode_info->mode = cur_mode;
	cur_mode_info->ud = mode_ud;

	{
		struct passwd* p = getpwuid(getuid());
		if(!p){
			p = malloc(sizeof(struct passwd));
			p->pw_name = "nobody";
			p->pw_gecos = "I don't know who I am!";
		}

		if(!irc_user){
			irc_user = p->pw_name;
		}

		if(!irc_nick){
			irc_nick = irc_user;
		}

		curNick = strdup(irc_nick);

		if(!irc_gecos){
			irc_gecos = p->pw_gecos;
		}
	}

	pthread_t thread1;
	pthread_create(&thread1, NULL, handle_input, cur_mode_info);

	char inBuffer[MAX_BUFFER_LEN+1];

	int doneReg = 0;

	int ret = 1;
	while(ret != -1 && ret != 0){
		ret = cur_mode->read(mode_ud, inBuffer, MAX_BUFFER_LEN);
		for(int i = 0; i < ret; i++){
			if(inBuffer[i] == '\r' || inBuffer[i] == '\n'){
				inBuffer[i] = '\0';
				break;
			}
		}

		if(!doneReg && strlen(inBuffer) > 0){
			int userLineLen = 14 + strlen(irc_user) + strlen(irc_gecos);
			char userLine[userLineLen];
			strcpy(userLine, "USER ");
			strcat(userLine, irc_user);
			strcat(userLine, " 8 * :");
			strcat(userLine, irc_gecos);

			rl_safe_printf(">>%s\n", userLine);
			strcat(userLine, "\r\n\0");

			cur_mode->write(mode_ud, userLine, userLineLen);

			int nickLineLen = 8 + strlen(irc_nick);
			char nickLine[nickLineLen];
			strcpy(nickLine, "NICK ");
			strcat(nickLine, irc_nick);

			rl_safe_printf(">>%s\n", nickLine);
			strcat(nickLine, "\r\n\0");

			cur_mode->write(mode_ud, nickLine, nickLineLen);

			doneReg = 1;
		}

		int inBufLen = strlen(inBuffer);

		if(ret > 0){
			rl_safe_printf("%s\n", inBuffer);

			char* subStr = strstr(inBuffer, "PING");
			if(subStr){
				int pos = subStr - ret;

				rl_safe_printf("Index in string: %i\n", pos);

				if(pos == 0){
					subStr = subStr + 5;

					int pongMsgLen = 8 + strlen(subStr);
					char toSend[pongMsgLen];
					strcpy(toSend, "PONG ");
					strcat(toSend, subStr);
					strcat(toSend, "\r\n");
					toSend[pongMsgLen] = '\0';

					cur_mode->write(mode_ud, toSend, pongMsgLen);
				}
			}
		}
	}

	stillConnected = 0;

	pthread_join(thread1, NULL);

	return EXIT_SUCCESS;
}
