/*
 * Copyright 2016 John M. Harris, Jr.
 *
 * This file is part of tIRC.
 *
 * tIRC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tIRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with tIRC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PLAIN_MODE_H_
#define PLAIN_MODE_H_

#include "tirc_mode.h"

struct tirc_mode_plain_ud{
	int sockfd;
};

/*
 * Initializes the plain transport mode, and returns a
 * struct of type tirc_mode_plain_ud.
 */
void* tirc_mode_plain_init(void* init_ud);

/*
 * Opens a connection to the specified address,
 * on the specified port.
 * ud is the userdata returned from tirc_mode_plain_init
 */
int tirc_mode_plain_open(void* ud, char* addr, char* port);

/*
 * Writes data to a specified connection.
 * ud is the userdata returned from tirc_mode_plain_init
 */
int tirc_mode_plain_write(void* ud, char* buf, int bufsize);

/*
 * Reads data from a specified connection.
 * ud is the userdata returned from tirc_mode_plain_init
 */
int tirc_mode_plain_read(void* ud, char* buf, int count);

/*
 * Closes a specified connection.
 * ud is the userdata returned from tirc_mode_plain_init
 */
int tirc_mode_plain_close(void* ud);

/*
 * Returns a struct of type tirc_mode.
 * This struct will be created with the values set to
 * the functions defined by this mode.
 */
struct tirc_mode* tirc_mode_plain();

#endif
