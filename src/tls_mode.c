/*
 * Copyright 2016 John M. Harris, Jr.
 *
 * This file is part of tIRC.
 *
 * tIRC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tIRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with tIRC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "tls_mode.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <unistd.h>

#include "tcp_util.h"

//This works for Fedora, not tested anywhere else
#define CAFILE "/etc/ssl/certs/ca-bundle.crt"

static int _verify_certificate_callback(gnutls_session_t session){
	struct tirc_mode_tls_init_ud* init_ud = (struct tirc_mode_tls_init_ud*)gnutls_session_get_ptr(session);

	if(!init_ud->verifyCerts){
		return 0;
	}

	unsigned int status;

	int ret = gnutls_certificate_verify_peers3(session, init_ud->hostname, &status);

	if(ret < 0 || status != 0){
		puts("Failed to verify certificate.");
		return GNUTLS_E_CERTIFICATE_ERROR;
	}else{
		puts("Successfully verified server certificate.");
		return 0;
	}
}

void* tirc_mode_tls_init(void* _init_ud){
	struct tirc_mode_tls_init_ud* init_ud = (struct tirc_mode_tls_init_ud*)_init_ud;

	struct tirc_mode_tls_ud* mode_ud = malloc(sizeof(struct tirc_mode_tls_ud));
	bzero(mode_ud, sizeof(struct tirc_mode_tls_ud));

	//Let's assume this is the first time GnuTLS is being used.
	gnutls_global_init();

	gnutls_init(&(mode_ud->session), GNUTLS_CLIENT);

	gnutls_certificate_credentials_t xcred;
	gnutls_certificate_allocate_credentials(&xcred);

	gnutls_certificate_set_x509_trust_file(xcred, CAFILE, GNUTLS_X509_FMT_PEM);
	gnutls_certificate_set_verify_function(xcred, _verify_certificate_callback);

	gnutls_session_set_ptr(mode_ud->session, init_ud);

	gnutls_set_default_priority(mode_ud->session);

	int ret = gnutls_priority_set_direct(mode_ud->session, "NORMAL", NULL);
	if(ret < 0){
		if(ret == GNUTLS_E_INVALID_REQUEST){
			puts("Invalid Request");
		}
		return NULL;
	}

	gnutls_credentials_set(mode_ud->session, GNUTLS_CRD_CERTIFICATE, xcred);

	gnutls_server_name_set(mode_ud->session, GNUTLS_NAME_DNS, init_ud->hostname, strlen(init_ud->hostname));

	if(init_ud != NULL){
		//TODO: Use the certificate specified
	}else{
		//gnutls_anon_client_credentials_t anoncred;
		//gnutls_anon_allocate_client_credentials(&anoncred);
		//gnutls_credentials_set(mode_ud->session, GNUTLS_CRD_ANON, anoncred);
	}

	return mode_ud;
}

int tirc_mode_tls_open(void* ud, char* addr, char* port){
	struct tirc_mode_tls_ud* mode_ud = (struct tirc_mode_tls_ud*)ud;

	mode_ud->sockfd = tcp_util_connect(addr, port);

	if(mode_ud->sockfd == -1){
		return 1;
	}

	//Connected, let's get going

	gnutls_transport_set_int(mode_ud->session, mode_ud->sockfd);
	gnutls_handshake_set_timeout(mode_ud->session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

	int ret;

	do{
		ret = gnutls_handshake(mode_ud->session);
	}while(ret < 0 && gnutls_error_is_fatal(ret) == 0);

	if(ret < 0){
		fputs("TLS handshake failed\n", stderr);
		gnutls_perror(ret);
		return 0;
	}

	return 0;
}

int tirc_mode_tls_write(void* ud, char* buf, int bufsize){
	struct tirc_mode_tls_ud* mode_ud = (struct tirc_mode_tls_ud*)ud;
	int ret = gnutls_record_send(mode_ud->session, buf, bufsize);

	if(ret < 0 && gnutls_error_is_fatal(ret) == 0){
		fprintf(stderr, "Warning: %s\n", gnutls_strerror(ret));
	}else if(ret < 0){
		fprintf(stderr, "Error: %s\n", gnutls_strerror(ret));
		return -1;
	}

	return ret;
}

int tirc_mode_tls_read(void* ud, char* buf, int count){
	struct tirc_mode_tls_ud* mode_ud = (struct tirc_mode_tls_ud*)ud;
	int ret = gnutls_record_recv(mode_ud->session, buf, count);

	if(ret < 0 && gnutls_error_is_fatal(ret) == 0){
		fprintf(stderr, "Warning: %s\n", gnutls_strerror(ret));
	}else if(ret < 0){
		fprintf(stderr, "Error: %s\n", gnutls_strerror(ret));
		return -1;
	}

	return ret;
}

int tirc_mode_tls_close(void* ud){
	struct tirc_mode_tls_ud* mode_ud = (struct tirc_mode_tls_ud*)ud;
	int ret = gnutls_bye(mode_ud->session, GNUTLS_SHUT_RDWR);
	gnutls_deinit(mode_ud->session);
	gnutls_global_deinit();
	return ret;
}

struct tirc_mode* tirc_mode_tls(){
	struct tirc_mode* mode = malloc(sizeof(struct tirc_mode));
	mode->init = tirc_mode_tls_init;
	mode->open = tirc_mode_tls_open;
	mode->write = tirc_mode_tls_write;
	mode->read = tirc_mode_tls_read;
	mode->close = tirc_mode_tls_close;

	return mode;
}
