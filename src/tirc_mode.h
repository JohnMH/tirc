/*
 * Copyright 2016 John M. Harris, Jr.
 *
 * This file is part of tIRC.
 *
 * tIRC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tIRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with tIRC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TIRC_MODE_H_
#define TIRC_MODE_H_

/*
 * Returns userdata to be passed to future use of this transport
 * mode. For SSL this could be a pointer to the current session
 * data. For plain this might be a struct holding a socket's fd,
 * or something of that nature.
 *
 * init_ud is different for every mode. For plain, it's NULL.
 * For SSL, it's either NULL or a string, path to a certificate
 * that will be used to connect to the server.
 *
 * Returns NULL on failure.
 */
typedef void* (*tirc_init_func)(void* init_ud);

/*
 * Opens the connection with given address and port. The ud
 * value is userdata created from the init function.
 * Returns 0 on success, 1 on failure.
 */
typedef int (*tirc_open_func)(void* ud, char* addr, char* port);

/*
 * Writes the specified amount of data, bufsize, to a
 * connection from buffer buf. The ud value is userdata
 * created from the init function.
 * Returns the amount of data written, -1 on failure.
 */
typedef int (*tirc_write_func)(void* ud, char* buf, int bufsize);

/*
 * Attempts to read up to count bytes from a connection
 * to the buffer buf. The ud value is userdata created from
 * the init function.
 * Returns the amount of data read, -1 on failure.
 */
typedef int (*tirc_read_func)(void* ud, char* buf, int count);

/*
 * Closes a connection as specified by the userdata
 * passed, ud.
 * Returns 0 on success, 1 on failure.
 */
typedef int (*tirc_close_func)(void* ud);

struct tirc_mode{
	tirc_init_func init;
	tirc_open_func open;
	tirc_write_func write;
	tirc_read_func read;
	tirc_close_func close;
};

#endif
